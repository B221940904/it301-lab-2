<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
session_start();
?>

<style>
    body {
        font-family: Arial, sans-serif;
    }
    table {
        width: 100%;
        border-collapse: collapse;
    }
    th, td {
        padding: 8px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    th {
        background-color: #4CAF50;
        color: white;
    }
</style>

<table>
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Email</th>
        <th>Password</th>
    </tr>
    <?php
   
    if (isset($_SESSION['users'])) {
       
        foreach ($_SESSION['users'] as $user) {
            echo "<tr>";
            echo "<td>{$user['name']}</td>";
            echo "<td>{$user['surname']}</td>";
            echo "<td>{$user['email']}</td>";
            echo "<td>{$user['password']}</td>";
            echo "</tr>";
        }
    }
    ?>
</table>
<form action="logout.php" method="post">
    <input type="submit" value="Logout">
</form>
</body>
</html>